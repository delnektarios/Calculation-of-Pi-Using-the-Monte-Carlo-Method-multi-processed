CC=gcc

CFLAGS=-c -w

LIBS=-lm 

all: execute
	rm -rf *o askisi

#number of processe to be created is equal
#to the number of arguments except the executable name
#and the last argument
execute: askisi
#		   side values and number of tries....
	./askisi 4 5 2 7 3 5 100

askisi: main.o functions.o
	$(CC) main.o functions.o -o askisi $(LIBS)

main.o:
	$(CC) $(CFLAGS) main.c

functions.o:
	$(CC) $(CFLAGS) functions.c -lm
