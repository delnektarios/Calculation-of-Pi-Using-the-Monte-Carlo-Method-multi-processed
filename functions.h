#ifndef FUNCTIONS //include guard
#define FUNCTIONS

#define SHMSIZE 15   // maximum number of bytes in a message

void cleanup (int semid,int shm_id, char *addr,char *message,char *side);

void output(int len,int tests,int* sides,int* all_sides,char a[tests][6]);

void calculation(char *side,char *message);

int shm_id; // ID of shared memory

#endif
