#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include "functions.h"
#include <time.h>


int main (int argc, char *argv[])
{

srand(time(NULL));

int tests=atoi(argv[argc-1]),children=argc-2,len=argc-2; // use of argument from cmd line
char *message,*side,buffer[10],a[tests][6];				      
int i,j,k,number,status;
int sides[tests],all_sides[len];

i=j=k=number=0;

for(j=0;j<len;j++) all_sides[j]=atoi(argv[j+1]);

message=malloc(sizeof(char)*(SHMSIZE));    //allocating space for messages
side=malloc(sizeof(char)*(SHMSIZE));
	
srand(time(NULL));

char *addr = (char *) malloc ((SHMSIZE) * sizeof (char));  //allocating space to be attached to shared mem segment

//seting up semaphores
union senum {
int  val;    
struct semid_ds *buf;    
unsigned short  *array;  
};

struct sembuf {
short sem_num;
short sem_op;
short sem_flg;
};

struct sembuf semopr[3];

int semid = semget((key_t) 333, 3, IPC_CREAT | 0660);
if(semid==-1) {printf("ALLOCATING THE SEMAPHORES = %s\n",strerror(errno)); exit(1);}
printf("ALLOCATING THE SEMAPHORES = %s\n",strerror(errno));

union senum semaphore1;
semaphore1.val=1;
semctl(semid, 0, SETVAL, semaphore1);

union senum semaphore2;
semaphore2.val=0;
semctl(semid, 1, SETVAL, semaphore2);
	
union senum semaphore3;
semaphore3.val=0;
semctl(semid, 2, SETVAL, semaphore3);

// seting up shared memory segment
if ((shm_id = shmget ((key_t) 1234, SHMSIZE, 0666 | IPC_CREAT)) == -1 ) {
        printf ("Error in shared memory region setup.\n");
        exit (2);
        } // if shared memory get failed

// attach the shared memory segment

addr = (char *) shmat (shm_id, (char *) 0, 0);
	
pid_t pid[children];
	
for(j=0;j<children;j++){
        pid[j]=fork();
        if (pid[j]>0) { // true if in parent process
                continue;
        } // end parent process
        else if (pid[j]==0){
                break;
        }//end of child proccess
        else {perror("fork"); cleanup (semid,shm_id, addr,message,side); exit(3);}
}//end of for

if( j == children ){
        k = 0;
        while ( k<tests )
        {
                number = 0 + rand() / (RAND_MAX / ((len-1) - 0 + 1) + 1); // random within range [0,argc-2]
                snprintf (side, sizeof(side), "%d",all_sides[number]);    //piching randomly a side
                semopr[0].sem_num = 0;
                semopr[0].sem_op = -1;     
                semopr[0].sem_flg = 0;
                semop(semid, &semopr[0], 1);
                printf("1 %s\n",strerror(errno));
                if (memcpy(addr,side,SHMSIZE)==NULL) { //sending request
                        puts ("Error in memory copy");
                        cleanup (semid,shm_id, addr,message,side);
                        exit (4);
                } // end if error in memory copy
                semopr[0].sem_num = 0;
                semopr[0].sem_op = 1;
                semopr[0].sem_flg = 0;
                semop(semid, &semopr[0], 1);
                //semaphore up to free access to shared memory
                printf("2 %s\n",strerror(errno));
                //this semaphore allows one of the children to read the request
                semopr[1].sem_num = 1;
                semopr[1].sem_op = 1;     
                semopr[1].sem_flg = 0;
                semop(semid, &semopr[1], 1);
                printf("3 %s\n",strerror(errno));
                //semaphore blocks the parent process to wait for the child's respond
                semopr[2].sem_num = 2;
                semopr[2].sem_op = -1;     
                semopr[2].sem_flg = 0;
                semop(semid, &semopr[2], 1);
                printf("3--m %s\n",strerror(errno));
                semopr[0].sem_num = 0; // gets the message sent by the child
                semopr[0].sem_op = -1;     
                semopr[0].sem_flg = 0;
                semop(semid, &semopr[0], 1);
                printf("4 %s\n",strerror(errno));
                if(strcpy (message, addr)==NULL){ cleanup (semid,shm_id, addr,message,side); exit(4);}
                semopr[0].sem_num = 0;
                semopr[0].sem_op = 1;
                semopr[0].sem_flg = 0;
                semop(semid, &semopr[0], 1);
                printf("5 %s\n",strerror(errno));
                //collecting and storing results from child proccesses
                i=0;
                while(*message!='\0' && isdigit(*message)) {buffer[i]=*message; i++; message++;} 
                sides[k]=atoi(buffer);
                message--;
                if(strchr(message,'t')==NULL) message=strchr(message,'f');
                else if(strchr(message,'f')==NULL) message=strchr(message,'t');
                strcpy(a[k],message);
                while(i>0){message--; i--;}                               
                k++;
        }//end of while

        //temination message
        snprintf (message,SHMSIZE,"%d",0);
        if (memcpy(addr,message,2)==NULL) {
                
                puts ("Error in memory copy");
                cleanup (semid,shm_id, addr,message,side);
                exit(4);
        }
        printf("exiting..........................................\n");
        for(j=0;j<children;j++){ //terminating children
	      semopr[1].sem_num = 1;
              semopr[1].sem_op = 1;     
              semopr[1].sem_flg = 0;
              semop(semid, &semopr[1], 1);
              printf("6 %s\n",strerror(errno));
	      pid[j]=wait(&status);
        }   
}
else if( pid[j] == 0 ){
// in child process
        printf("i am a child with proccess id  =%d\n",getpid());
        while(1){ //keeping the child alive
                // semdown to hold waiting the chance to grab a request from parent process
                semopr[1].sem_num = 1;
                semopr[1].sem_op = -1;     
                semopr[1].sem_flg = 0;
                semop(semid, &semopr[1], 1);
                printf("7 %s\n",strerror(errno));
                printf("i am a child with proccess id  =%d\n",getpid());
                //semaphore down to access shared memory
                semopr[0].sem_num = 0;
                semopr[0].sem_op = -1;     
                semopr[0].sem_flg = 0;
                semop(semid, &semopr[0], 1);
                printf("8 %s\n",strerror(errno));
                if(strcpy (side,addr)==NULL){ cleanup (semid,shm_id, addr,message,side); exit(4);}
                semopr[0].sem_num = 0;
                semopr[0].sem_op = 1;
                semopr[0].sem_flg = 0;
                semop(semid, &semopr[0], 1);
                printf("9 %s\n",strerror(errno));
                //in case of termination order from parent process
                if (atoi(side)==0) exit(0);
                //calculation
                calculation(side,message);
                //child sends message using shared memory
                //copies two arrays in consecutive order in shared memory
                semopr[0].sem_num = 0;
                semopr[0].sem_op = -1;     
                semopr[0].sem_flg = 0;
                semop(semid, &semopr[0], 1);
                printf("10 %s\n",strerror(errno));
                //child sends responce
                if (memcpy(addr,side,SHMSIZE) == NULL) {
                        puts ("Error in memory copy");
                        cleanup (semid,shm_id, addr,message,side);
                        exit (4);
                } // end if error in memory copy
                semopr[0].sem_num = 0;
                semopr[0].sem_op = 1;
                semopr[0].sem_flg = 0;
                semop(semid, &semopr[0], 1);
                printf("11 %s\n",strerror(errno));
                //confirmation that parent process has an answer and can proceed to next request
                semopr[2].sem_num = 2;
                semopr[2].sem_op = 1;     
                semopr[2].sem_flg = 0;
                semop(semid, &semopr[2], 1);
                printf("3--c %s\n",strerror(errno));
        }               
}
output(len,tests,sides,all_sides,a);      //printing results
cleanup (semid,shm_id,addr,message,side); //cleaning up all memory segments and semaphores
exit(0);
} // end main program









