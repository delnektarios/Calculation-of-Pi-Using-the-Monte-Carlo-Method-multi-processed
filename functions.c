#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include "functions.h"


// remove shared memory segment
void cleanup (int semid,int shm_id, char *addr,char *message,char *side)
{
        semctl(semid, 0, IPC_RMID,0);
        printf("\nSEMAPHORES REMOVED = %s\n",strerror(errno));
        printf("\nREMOVING SHARED MEMORY = %s\n\n\n",strerror(errno));
        shmdt (addr);
        shmctl (shm_id, IPC_RMID, 0);
        return;
} // end cleanup function

//prints results of all calculations of pi
void output(int len,int tests,int sides[],int all_sides[],char a[][6])
{
	int i,j,k,sum,sum1;
	double o_i;
	printf("in output.....................\n");
	for(j=0;j<len;j++){
		sum=sum1=0;
		for(k=0;k<tests;k++){
			if( all_sides[j]==sides[k] ){ sum++; if(strcmp(a[k],"true")==0) sum1++; }		 	
		}
		if(sum!=0){ o_i=4*((double)sum1)/sum; printf("pi=%f\n",o_i); }		
	}
        return;	
}//end of output function

//calculates pi
void calculation(char *side,char *message)
{
        double s,d,x,y;
        s=(atoi(side))/2;
        x=(double)rand()/(double)RAND_MAX * (s - 0) + 0;
        y=(double)rand()/(double)RAND_MAX * (s - 0) + 0;
        d = sqrt(x*x+y*y);
        if (d<=s){ snprintf (message,SHMSIZE,"%s","true");}
        else { snprintf (message,SHMSIZE,"%s","false");}
        strcat(side,message); //copies two arrays in consecutive order in shared memory
        return;
}// end of calculation function
